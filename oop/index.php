<?php 

require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$object = new Animal("shaun");

echo "Name : $object->name <br>" ;
echo "Legs : $object->legs <br>";
echo "Cold Blooded : $object->cold_blooded <br> <br>";

$kodok = new Frog("buduk");

echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
$kodok->jump();

$sungokong = new Ape("kera sakti");

echo "Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold Bloooded : $sungokong->cold_blooded <br>";
$sungokong->yell();


?>