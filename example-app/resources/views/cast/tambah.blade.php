@extends('layout.master')

@section('judul')
Halaman Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input name="nama" type="text" value="{{old('nama')}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input name="umur" type="integer" value="{{old('umur')}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio Cast</label>
        <textarea name="bio"  class="form-control" cols="30" rows="10">{{old('bio')}}</textarea>
      </div>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection